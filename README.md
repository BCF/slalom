## SLALOM

**NOTE: SLALOM has been migrated to https://gitlab.com/habermannlab/slalom and is discontinued on framagit**

SLALOM (StatisticaL Analysis of Locus Overlap Method) is a standalone software tool for analysis of positional data. Namely, it compares two distinct lists of site intervals (hereafter – sites; in the associated publication referred to as continuous sequence elements - CSEs) in a given set of sequences to evaluate the overlap quality according to rules set by the user. The sites are presented as sequence coordinates, with the start and end positions, while the sequences can be of any nature (e.g., alphabetical or time series).

For more information, please, read the user manual.

This work was partially funded by the IFB project [T5](https://www.france-bioinformatique.fr/fr/projets2015/t5-project).
